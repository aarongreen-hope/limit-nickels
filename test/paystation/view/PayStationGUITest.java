package paystation.view;

import javax.swing.*;

/** This pay station gui adds an additional menu
    for visual testing the Gamma town variant.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class PayStationGUITest extends PayStationGUI {

  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch ( Exception e ) {
      // ignore silently, as we then just do not get the required
      // look and feel for all windows
    }
    new PayStationGUITest();
  }
 
  protected void addMoreProductVariants(JMenu menu) {
    JMenuItem menuItem;
    menuItem = makeTownMenuItem("Gammatown (Test)", 
                                new GammaTownVisualTestFactory() );
    menu.add(menuItem);
  }
}

