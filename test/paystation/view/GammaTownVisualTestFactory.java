package paystation.view;

import paystation.domain.*;
import paystation.domain.factory.GammaTownFactory;
import paystation.domain.strategy.rate.AlternatingRateStrategy;
import paystation.domain.strategy.rate.LinearRateStrategy;
import paystation.domain.strategy.rate.ProgressiveRateStrategy;
import paystation.domain.strategy.rate.RateStrategy;

/** Factory to configure GammaTown for visual testing.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class GammaTownVisualTestFactory extends GammaTownFactory {
  // reconfigure to use the visual dialog box for asking if it
  // is weekend or not...
  public RateStrategy createRateStrategy() {
    return new AlternatingRateStrategy( new LinearRateStrategy(),
                                        new ProgressiveRateStrategy(),
                                        new DialogDecisionStrategy() );
  }
}