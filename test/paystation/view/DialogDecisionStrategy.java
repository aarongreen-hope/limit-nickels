package paystation.view;

import paystation.domain.*;
import paystation.domain.strategy.weekend.WeekendDecisionStrategy;

import javax.swing.*;

/** A visual test stub for the weekend decision strategy.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/

public class DialogDecisionStrategy implements WeekendDecisionStrategy {
  public boolean isWeekend() {
    return
      JOptionPane.YES_OPTION
      ==
      JOptionPane.showConfirmDialog(null,
                                    "Is it weekend?",
                                    "WeekendDecisionStrategy",
                                    JOptionPane.YES_NO_OPTION );
  }
}

