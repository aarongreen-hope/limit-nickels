package paystation.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import paystation.domain.decorator.LimitNickelsPayStation;
import paystation.domain.factory.PayStationFactory;
import paystation.domain.receipt.Receipt;
import paystation.domain.receipt.StandardReceipt;
import paystation.domain.strategy.display.DisplayStrategy;
import paystation.domain.strategy.display.ValueDisplayStrategy;
import paystation.domain.strategy.rate.RateStrategy;

public class TestLimitNickelsDecorator {

	private PayStation ps;
	
	@Before
	public void setUp() throws Exception {
		ps = new LimitNickelsPayStation(new PayStationImpl(new PayStationFactory(){

			@Override
			public RateStrategy createRateStrategy() {
				return new One2OneRateStrategy();
			}

			@Override
			public Receipt createReceipt(int parkingTime) {
				return new StandardReceipt(parkingTime);
			}

			@Override
			public DisplayStrategy createDisplayStrategy() {
				return new ValueDisplayStrategy();
			}
			
		}));
	}

	@Test
	public void testPayStationAcceptsFirstTenNickels() throws IllegalCoinException{
		int balance = 0;
		for(int i = 0; i < 10; i++){
			ps.addPayment(5);
			balance = ps.readDisplay();
			assertEquals("The first ten nickels should add value to the transaction", 5 * (i + 1), balance);
		}
	}
	@Test (expected = IllegalCoinException.class)
	public void testPayStationRejectsAdditionalNickels() throws IllegalCoinException{
		for(int i = 0; i < 11; i++){
			ps.addPayment(5);
		}
	}

	@Test
	public void testPayStationDoesNotAddValueOfRejectedNickels() throws IllegalCoinException{
		for(int i = 0; i < 10; i++){
			ps.addPayment(5);
		}
		try{
			ps.addPayment(5);
		}catch(IllegalCoinException ice){
			assertEquals("Rejected nickels should not add value to the transaction", 50, ps.readDisplay());
		}
	}
}
