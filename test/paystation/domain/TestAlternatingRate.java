package paystation.domain;

import org.junit.*;

import paystation.domain.strategy.rate.AlternatingRateStrategy;
import paystation.domain.strategy.rate.LinearRateStrategy;
import paystation.domain.strategy.rate.ProgressiveRateStrategy;
import paystation.domain.strategy.rate.RateStrategy;

import static org.junit.Assert.*;

/** Test all aspects of the Gamma rate policy.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class TestAlternatingRate {
  /** Test two hour parking during weekdays */
  @Test public void shouldDisplay120MinFor300centWeekday() {
    RateStrategy rs = 
      new AlternatingRateStrategy( new LinearRateStrategy(),
                                   new ProgressiveRateStrategy(),
                                   new FixedDecisionStrategy(false) );
    assertEquals( 300 / 5 * 2, rs.calculateTime(300) ); 
  }
  /** Test two hour parking during weekends */
  @Test public void shouldDisplay120MinFor350centWeekend() {
    RateStrategy rs = 
      new AlternatingRateStrategy( new LinearRateStrategy(),
                                   new ProgressiveRateStrategy(),
                                   new FixedDecisionStrategy(true) );
    assertEquals( 300 / 5 * 2, rs.calculateTime(350) );
  }
} 
