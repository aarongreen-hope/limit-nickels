package paystation.domain.decorator;
import java.util.Date;

import paystation.domain.IllegalCoinException;
import paystation.domain.PayStation;
import paystation.domain.receipt.Receipt;
/** A  PayStation decorator that logs coin entries.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class LogDecorator implements PayStation {
  private PayStation paystation;
  public LogDecorator( PayStation ps ) {
    paystation = ps;
  }
  public void addPayment( int coinValue ) 
          throws IllegalCoinException {
    System.out.println( ""+coinValue+" cents: "+new Date() );
    paystation.addPayment( coinValue );
  }
  public int readDisplay() { return paystation.readDisplay(); }
  public Receipt buy() { return paystation.buy(); }
  public void cancel() { paystation.cancel(); }
}

