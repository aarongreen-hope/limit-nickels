package paystation.domain.decorator;

import paystation.domain.IllegalCoinException;
import paystation.domain.PayStation;
import paystation.domain.receipt.Receipt;

public class LimitNickelsPayStation implements PayStation {

	private PayStation paystation;
	private int nickelCount;
	public LimitNickelsPayStation(PayStation ps){
		paystation = ps;
		nickelCount = 0;
	}
	
	@Override
	public void addPayment(int coinValue) throws IllegalCoinException {
		if(nickelCount >= 10 && coinValue == 5){
			throw new IllegalCoinException("Too many nickels inserted for this transaction");
		}else{
			if(coinValue == 5) nickelCount++;
			paystation.addPayment(coinValue);
		}

	}

	@Override
	public int readDisplay() {
		return paystation.readDisplay();
	}

	@Override
	public Receipt buy() {
		nickelCount = 0;
		return paystation.buy();
	}

	@Override
	public void cancel() {
		nickelCount = 0;
		paystation.cancel();
	}

}
