package paystation.domain.factory;

import paystation.domain.receipt.Receipt;
import paystation.domain.receipt.StandardReceipt;
import paystation.domain.strategy.display.DisplayStrategy;
import paystation.domain.strategy.display.ValueDisplayStrategy;
import paystation.domain.strategy.rate.ProgressiveRateStrategy;
import paystation.domain.strategy.rate.RateStrategy;

/** Factory to configure BetaTown.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class BetaTownFactory implements PayStationFactory {
  public RateStrategy createRateStrategy() {
    return new ProgressiveRateStrategy();
  }
  public Receipt createReceipt( int parkingTime ) {
    return new StandardReceipt(parkingTime, true);
  }
  public DisplayStrategy createDisplayStrategy() {
    return new ValueDisplayStrategy();
  }
}